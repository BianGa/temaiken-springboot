package com.utn.temaiken.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class InitController {

    @RequestMapping("/index")
    public String index(){
        return "index";
    }

}
